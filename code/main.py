import numpy as np
import hashlib
import secrets

#####################
##### Constants #####
#####################

k = 10

#####################
#####################
#####################



# Creates an array containing the individual bits of n
def toBitArray(n):
    return [int(digit) for digit in bin(n)[2:]]

# Converts a bitstring to a set ( S := { 2i - x_i | 1 ≤ i ≤ l } ⊂ {1, ..., 2l} )
def toSet(bitstring):
    bitArray = toBitArray(bitstring)
    S = set()
    l = len(bitArray)

    for i, bit in enumerate(bitArray):
        S.add(2*(i + 1) - bit)

    return S, l

# Symmetric Difference between Two Sets
def ssd(S_0, S_1):
    union = S_0.union(S_1)
    intersection = S_0.intersection(S_1)
    return union.difference(intersection)

def ssd_t(S_0, S_1, t):
    return len(ssd(S_0, S_1)) >= t

# Deterministic r_i based on sha256
def r_i(x, i, t):
    m = hashlib.sha256()
    m.update(str(x + i).encode('UTF-8'))
    hashInt = int(m.hexdigest(), 16)
    return hashInt % (2*t)

# Main hash function
def hash(x):
    m = hashlib.sha3_256()
    m.update(str(x).encode('UTF-8'))
    hashInt = int(m.hexdigest(), 16)
    return hashInt

# Decode hash using exhaustive search
def decodeHash(hashInt, l):
    hashInt = abs(hashInt)
    for x in range(2*l + 1):
        if hashInt == hash(x):
            return x
        
    return -1

# "Sample" function from the paper
def sample(t):
    
    R = []
    for i in range(k):
        R.append(
            lambda x, i=i+secrets.randbelow(2*t): r_i(x, i, t) # i=i so i is local to the lambda and doesn't change
        )

    A = hash

    return (R, A, t) # added to so we don't have to pass it as a separate argument


# "Encode" function from the paper
def encode(f, S):
    (R, A, t) = f
    
    # H := (0n)k×2t ∈ (Znq)k×2t
    H = np.zeros((k, 2*t), dtype='object') # Type object so that it can store numbers of infinite size

    # foreach x ∈ X
    for x in S:
        # foreach i ∈ [k]
        for i in range(k):
            # H[i, ri(x)] := H[i, ri(x)] + Aeᵀx
            r_i = R[i]
            H[i, r_i(x)] = H[i, r_i(x)] + A(x)

    return H

# "Peel" function as defined in the paper
def peel(f, H, Z):
    R, _, _ = f

    H_new = np.array(H, copy=True) # Clone H, so this method doesn't become destructive
    
    # foreach (x, w) ∈ Z
    for (x, w) in Z: # x is value, w is hash
        # foreach i ∈ [k]
        for i in range(k):
            # H[i, ri(x)] := H[i, ri(x)] − w
            r_i = R[i]
            H_new[i, r_i(x)] = H_new[i, r_i(x)] - w

    return H_new
            
# "Decode" function as defined in the paper
def decode(f, H_0, H_1, l_0, l_1):

    _, _, t = f    
    l = max(l_0, l_1)

    # H := H0 − H1
    H = np.subtract(H_0, H_1)
    # X' := ∅
    X_prime = set()

    while True:
        # Z := { ... }
        Z = set()
        for i in range(k):
            for j in range(2*t):
                w = H[i, j]
                x = decodeHash(w, l)
                if (x >= 0): Z.add((x, w)) # Decoded successfully

        # X' := X' U { x | ∃w. (x, w) ∈ Z }
        for (x, w) in Z:
            X_prime.add(x)

        # H := Peel(f, H, Z)
        H = peel(f, H, Z)

        # while Z =/= ∅
        if (len(Z) == 0): break

    # if H = (0n)k×2t
    if not np.any(H):
        # H contains only zeros
        
        # return X'
        return X_prime
    else:
        # return ⊥
        return False


# Returns true if hamming distance larger or equal to t
def evaluate(H_0, H_1, l_0, l_1, f):

    _, _, t = f

    X_prime = decode(f, H_0, H_1, l_0, l_1)

    print(f"Decoded SSD: {sorted(X_prime) if X_prime else False}")

    if (not X_prime): return True # Decoding failed -> hamming distance too large

    # Decoding successfull -> got symmetric set difference -> can calculate hamming distance
    return len(X_prime) >= t


def test(S_0, S_1, l_0, l_1, t, expected):
    f = sample(t)
    H_0 = encode(f, S_0)
    H_1 = encode(f, S_1)

    # Print header
    print(f"#### t = {t} (expected: {expected}) ####")

    # Evaluate hashes
    eval = evaluate(H_0, H_1, l_0, l_1, f)
    print(f"Evaluated hash:     {eval}")

    # Evaluate original
    evalOriginal = ssd_t(S_0, S_1, t)
    print(f"Evaluated original: {evalOriginal}\n")


if __name__ == "__main__":

    ################################
    #### Small hamming distance ####
    ################################

    print("################################\n#### Small hamming distance ####\n################################")

    X_0 = 0b110110101110111010000101110101
    X_1 = 0b110111101110101010000101110101
    S_0, l_0 = toSet(X_0)
    S_1, l_1 = toSet(X_1)

    ssd_original = ssd(S_0, S_1)
    print(f"Original symmetric set difference: {sorted(ssd_original)} (distance = {len(ssd_original)})\n")


    test(S_0, S_1, l_0, l_1, t=3, expected=True)
    test(S_0, S_1, l_0, l_1, t=4, expected=True)
    test(S_0, S_1, l_0, l_1, t=5, expected=False)
    test(S_0, S_1, l_0, l_1, t=1000, expected=False)
    print("\n\n")


    ################################
    #### Large hamming distance ####
    ################################

    print("################################\n#### Large hamming distance ####\n################################")
    
    X_0 = 0b110000101001111001111000110101
    X_1 = 0b101111101110101010000101000001
    S_0, l_0 = toSet(X_0)
    S_1, l_1 = toSet(X_1)

    ssd_original = ssd(S_0, S_1)
    print(f"Original symmetric set difference: {sorted(ssd_original)} (distance = {len(ssd_original)})\n")


    test(S_0, S_1, l_0, l_1, t=3, expected=True)
    test(S_0, S_1, l_0, l_1, t=38, expected=True)
    test(S_0, S_1, l_0, l_1, t=39, expected=False)
    test(S_0, S_1, l_0, l_1, t=1000, expected=False)
    print("\n\n")